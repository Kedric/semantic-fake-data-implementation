# CPDB (Correctness Protected Database)

Our project is creating a database scheme that allows data owners to ensure that their queries are completely returned.
We use order preserving encryption and binary search to differentiate between fake and real data. The fake data is then 
filtered out and counted. Then we compare the expected amount against the counted amount, if it is equal then all the
data is there, otherwise the data is incomplete.

## Getting Started

First start by cloning the [implementation](https://gitlab.com/Kedric/semantic-fake-data-implementation)

Take the jar in /out and put it inside OPE/cryptdb/crypto

Read the [instructions](OPE/Readme.txt) on how to setup the OPE implementation.

### Prerequisites

Requires CentOS.

Requires [MongoDB](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/)

To start the mongo server:
```
$mongod
```

### Installing

Running the jar
```
$java -jar Implementation.jar
```

Answer the questions given by the program. For first time use, answer true each time.

You will need the names.yml located in resources of this project to setup the names database for the formatting function.



## Running the tests

To perform a test on this program, simply remove a piece of fake data to make sure that it returns that one of the
documents are missing.

Do the following commands in bash.
```
$mongo
$use admin
$db.employees.remove({"_id": "<id of fake tuple>"})
```

## Built With

* [MongoDB](https://www.mongodb.com/) - Database software
* [Morphia](https://mongodb.github.io/morphia/) - Query library

## Authors

* **Kedric Salisbury** - *Initial work* - [@Kedric](https://gitlab.com/Kedric)

## License

This project is licensed under the MIT License

## Acknowledgments

* Researchers - Lois Urizar, Holly Roisum, Megan Magette

* Dr. Yeh - Our Mentor