package me.Kedric.Implementation;

import org.mongodb.morphia.annotations.*;

import java.util.Date;

/**
 * Created by Kedric Salisbury 6/14/20176/14/2017
 */
@Entity(value = "employees", noClassnameStored = true)
@Indexes(
        @Index(value = "firstName", fields = @Field(value = "firstName"))
)
public class Employee {
    @Indexed
    String firstName;
    @Id
    long id;
    Date dateOfBirth;
    public Employee(){

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public Date setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return dateOfBirth;
    }

    public long getEmployeeUUID() {
        return id;
    }

    public void setEmployeeUUID(long employeeUUID) {
        this.id = employeeUUID;
    }

    @Override
    public String toString() {
        return id + " : " + firstName + " : " + dateOfBirth.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return ((Employee) obj).id == id || ((Employee) obj).dateOfBirth.equals(dateOfBirth);
    }
}
