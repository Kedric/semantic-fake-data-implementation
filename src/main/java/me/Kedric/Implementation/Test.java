package me.Kedric.Implementation;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kedri on 7/21/2017.
 */
public class Test {
    public static void main(String[] args){
        Date date = new Date();
        System.out.println(date.getTime());
        Date oldDate = new Date(70, 0, 1);
        Date oldDate2 = new Date(110,0,1);
        System.out.println(oldDate.getTime());
        System.out.println(oldDate2.getTime());

        System.out.println(Math.log(1970)/Math.log(2));
        ///                         1099511627776
        System.out.println(BigDecimal.valueOf(Math.pow(2,40)).toBigInteger());
        long l = 1099511627776L+25200000;
        System.out.println(l);
        System.out.println(l < 1262329200000L);
        System.out.println(new Date(l).toString());
    }
}
