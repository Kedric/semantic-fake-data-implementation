package me.Kedric.Implementation;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.io.*;
import java.lang.reflect.Field;
import java.security.InvalidParameterException;
import java.util.*;

/**
 * @author Kedric Salisbury
 */
public class Main {
    /**
     * Handles all of the revocations
     */
    static List<Integer> revoked = new ArrayList<>();
    /**
     * Data that is generated will use this seed.
     */
    static long seed = 105285;
    public static void main(String[] args){

        loadFile();
        final Morphia morphia = new Morphia();
        morphia.mapPackage("Implementation");
        mc = new MongoClient();
        datastore = morphia.createDatastore(mc, "admin");
        datastore.ensureIndexes();
        Scanner scanner = new Scanner(System.in);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Would you like to save names? true/false");
        boolean b = scanner.nextBoolean();
        if (b)saveNames();
        System.out.println("Would you like to insert tuples for testing purposes? (160 currently) true/false");
        boolean b2 = scanner.nextBoolean();
        if (b2){
            System.out.println("How many would you like to insert?");
            insert(scanner.nextInt());
        }
        System.out.println("Would you like to insert fake tuples (currently only works for 16)");
        boolean b3 = scanner.nextBoolean();
        if (b3)insertFakeData(4);
        seed = 40;
        //2^29

        System.out.println("Specify a lower bound: ");
        int lower = scanner.nextInt();
        System.out.println("Specify an upper bound: ");
        int upper = scanner.nextInt();
        if (lower < 0)throw new InvalidParameterException("You must specify a lower bounds that is greater than 0.");
        if (upper>999999999)throw new InvalidParameterException("You must specify an upper bounds that is less that 999999999.");
        if (lower > upper)throw new InvalidParameterException("You must specify a bounds lower than the upper bounds.");
        List<Employee> emps = query(4, 29, 100000000, "id", lower, upper);
        for (Employee emp : emps){
            System.out.println(emp);
        }
        List<Employee> empd = query(4,40,25200000,"dateOfBirth",0,1500910158762L);
        for (Employee emp : empd){
            System.out.println(emp);
        }
        mc.close();
    }
    /**
     * Queries the database from lower and upper dates
     * @param b1 the first bit size (domain)
     * @param b2 the second bit size (range)
     * @param add the added range that the bits can generate
     * @param field the field being queried
     * @param lower bound
     * @param upper bound
     * @return list of queried employees sorted by date of birth
     */
    public static List<Employee> query(int b1, int b2, long add, String field, long lower, long upper){
        List<Employee> employees;
        if (field.toLowerCase().contains("date")) {
            employees=datastore.createQuery(Employee.class).order(field).field(field).lessThanOrEq(new Date(upper)).field(field).greaterThanOrEq(new Date(lower)).asList();
        }else{
            employees=datastore.createQuery(Employee.class).order(field).field(field).lessThanOrEq(upper).field(field).greaterThanOrEq(lower).asList();
        }
        int d1 = binarySearch(b1, b2, add, lower);
        System.out.println("Binary Searching: " + lower);
        int d2 = binarySearch(b1, b2, add, upper);
        System.out.println("Binary Searching: " + upper);
        if (encrypt(d1, b1, b2,add) <= lower){
            d1++;
        }
        if (encrypt(d2,b1,b2,add) >= upper){
            d2--;
        }
        System.out.println("d1 = " + d1);
        System.out.println("d2 = " + d2);
        int fake = 0;
        for (int i = d1; i <= d2; i++) {
            Employee emp = new Employee();
            try {
                Field f = emp.getClass().getDeclaredField(field);
                f.setAccessible(true);
                if(field.toLowerCase().contains("date")){
                    f.set(emp,new Date(encrypt(i,b1,b2,add)));
                }else{
                    f.set(emp, encrypt(i, b1, b2, add));
                }
                if (employees.contains(emp)){
                    employees.remove(emp);
                    fake++;
                    System.out.println("Contains " + f.get(emp).toString());
                }else{
                    System.out.println("Missing " + encrypt(i,b1,b2,add));
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                continue;
            }

        }
        System.out.println(fake + " ?== " + ((d2-d1)+1));
        if (fake == d2-d1+1){
            System.out.println("The data is complete.");
        }else{
            System.out.println("The data is not complete.");
        }
        return employees;
    }

    /**
     * Inserts fake tuples of bit size b
     * @param b bit size
     */
    public static void insertFakeData(int b){
        for (int i = 0; i < Math.pow(2, b); i++) {
            insertFakeTuple(i);
        }
    }

    /**
     * Gets the next long in the random from https://stackoverflow.com/questions/2546078/java-random-long-number-in-0-x-n-range
     * @param rng random number generator from java. Used to generate a long
     * @param n the range
     * @return random long in range
     */
    static long nextLong(Random rng, long n) {
        // error checking and 2^x checking removed for simplicity.
        long bits, val;
        do {
            bits = (rng.nextLong() << 1) >>> 1;
            val = bits % n;
        } while (bits-val+(n-1) < 0L);
        return val;
    }
    //Found on stack overflow
    /**
     * Inserts the amount specified. Generates fake data for testing purposes.
     * @param amount
     */
    public static void insert(int amount){
        Random random = new Random(seed);
        // List<me.Kedric.Implementation.Employee> employees = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            int id = random.nextInt(999999999-100000000) + 100000000;
            long date = nextLong(random, 1262329200000L-25200000)+25200000;
            Employee employee = new Employee();
            Name nameIndex = getStringFromDatabase(id);
            employee.firstName = nameIndex.name;
            employee.setEmployeeUUID(id);
            employee.setDateOfBirth(new Date(date));
            datastore.save(employee);
        }
        //USE new Date(cipher);
    }

    /**
     * Inserts a fake tuple into the database and formats the id into a name
     * @param i used to format into a string and set as the employee's id
     */
    public static void insertFakeTuple(int i){
        // List<me.Kedric.Implementation.Employee> employees = new ArrayList<>();
        long id = encrypt(i,4,29,100000000);
        long date = encrypt(i,4,40,25200000);
        Employee employee = new Employee();
        Name nameIndex = getStringFromDatabase(id);
        employee.firstName = nameIndex.name;
        employee.setEmployeeUUID(id);
        employee.setDateOfBirth(new Date(date));
        datastore.save(employee);
    }
    /**
     * Reads names file and saves them into mongodb
     * Eventually we want to outsource this database
     * For this prototype we will use our own.
     */
    public static void saveNames(){
        int i = 0;
        try {
            BufferedReader fr = new BufferedReader(new FileReader(new File("names.yml")));
            String s = fr.readLine();
            while(s!= null && !s.equals("")){
               Name name = new Name(s);
                name.id = Name.nextID++;
                datastore.save(name);
                s = fr.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds the revoked fake data to the revoked list.
     */
    public static void loadFile(){
        revoked.clear();
        File file = new File("revoked.txt");
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedReader fr = new BufferedReader(new FileReader(file));
            fr.lines().forEach(i ->{
                try {
                    revoked.add(Integer.parseInt(i));
                }catch (NumberFormatException e){
                    e.printStackTrace();
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static Datastore datastore;
    static MongoClient mc;

    /**
     * Formats the id to a name.
     * @param index or id
     * @return the name found at said index.
     */
    public static Name getStringFromDatabase(long index){
        Name name = datastore.createQuery(Name.class).field("id").equal(index%5163).get();
        return name;
    }

    /**
     * Searches the array for the closest encrypted data with id @param key
     * @param b1 the first bit size (domain)
     * @param b2 the second bit size (range)
     * @param add the added range that the bits can generate
     * @param key the key that the binary search will find the closest to.
     * @return the index of the closest key in inputArr
     */
    public static int binarySearch(int b1, int b2, long add, long key) {
        int[] inputArr = new int[(int) Math.pow(2, 4)];
        for (int i = 0; i < inputArr.length; i++) {
            inputArr[i] = i;
        }
        int start = 0;
        int end = inputArr.length - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (key == (encrypt(inputArr[mid],b1,b2,add))) {
                return mid;
            }
            if (key < (encrypt(inputArr[mid],b1,b2,add))) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        System.out.println("end=" + end +"\nstart=" + start);
        if (end < 0){
            end=0;
        }
        if (end > inputArr.length){
            end--;
        }
        return inputArr[end];
    }
    /**
     * Encrypts the data to a cipher
     * @param plain the plain data to encrypt
     * @return the cipher returned
     */
    public static long encrypt(int plain, int n1, int n2, long add) {
        try {
            File file = new File("/mnt/backup/icdb2015/OPE/cryptdb/crypto/","key.txt");
            file.createNewFile();
            FileWriter f = new FileWriter(file);
            BufferedWriter fw = new BufferedWriter(f);
            fw.write("thisisakey");
            fw.newLine();
            fw.write(String.valueOf(n1));
            fw.newLine();
            fw.write(String.valueOf(n2));
            fw.close();
            f.close();
            ProcessBuilder pb = new ProcessBuilder("a.out", "-e", String.valueOf(plain));
            pb.redirectErrorStream(true);
            Process p = pb.start();


            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            String returned = "";
            p.waitFor();
            while ((line = reader.readLine()) != null) {
                if (line.contains("The cipher is")) {
                    returned = line.replaceAll("The cipher is: ", "");
                    break;
                }
            }
            p.destroy();
            if (Objects.equals(returned, "")) {
                return 0;
            }
            return Long.valueOf(returned)+add;
        }catch (InterruptedException | IOException ignored){

        }
        return 0;
    }
}
