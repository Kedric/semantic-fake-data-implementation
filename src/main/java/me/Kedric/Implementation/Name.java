package me.Kedric.Implementation;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

/**
 * Created by Kedric Salisbury on 6/14/2017.
 */
@Entity(value = "names", noClassnameStored = true)
@Indexes(
        @Index(value = "name")
)
public class Name {
    @Id
    int id;
    static int nextID= 0;
    String name;
    public Name(){

    }
    public Name(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
