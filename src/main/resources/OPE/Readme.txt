
To compile with the command:
cd cryptdb/crypto
g++ -std=c++0x -I./../../ntl/include -L./../../ntl/lib -L./../../openssl/lib -I./../../openssl/include -I./../ ope.cc hgd.cc -lcrypto -lssl -lntl -lgmp -lm


Sample usage:
./a.out -e 10                     
-------- output -----------         
The plainText is: 10                  
The cipher is: 199942099045
----------------------------

./a.out -d 199942099045
-------- out put -----------
The cipher is: 199942099045
The plainText is: 10
----------------------------


To configure, change the settings in the key.txt file.
There are three lines in the txt file.
for example:
**********************************
samplekey        // key
64               //plaintext bits
128              // cipher text bits

NOTICE:
1. It seems that for this OPE algorithm I extra from the package, it only works with numbers. 
2. In the key.txt file, the plaintext bits and cipher text can not be the same, otherwise the plaintext and cipher will be the same.
3. And seems that the cipher bits should be longer than plaintext bits
4. For same setting, the same plaintext will come to same cipher .


